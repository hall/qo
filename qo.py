#!/bin/python
from qiskit import *
from pathlib import Path
import argparse
import math
import os
import string
import signal
import sys

# TODO: save file and allow reload
def signal_handler(sig, frame):
        print('\nuntil next time!')
        sys.exit(0)
signal.signal(signal.SIGINT, signal_handler)


"""
Do any necessary setup (create files/directories as needed).
"""
def setup():
    directory = Path.home() / '.go'
    if not os.path.exists(directory):
        os.makedirs(directory)


parser = argparse.ArgumentParser(
    description='quantum go',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('--size', type=int, default="19", nargs="?", help='board size')
parser.add_argument('--game', type=argparse.FileType('w+'), default=str(Path.home()/ '.go' / 'game'), nargs="?", help='load game from file')
args = parser.parse_args()

samples = 1024

q = QuantumRegister(args.size)
c = ClassicalRegister(args.size)
qc = QuantumCircuit(q, c)

qc.u3(math.pi,0,0, q[0])
qc.measure( q[0], c[0] )

# initialize empty board as a list of dicts (keyed by alphabetic characters)
board = [{y: '|' for y in (string.ascii_lowercase[:args.size])} for x in range(args.size)]

"""
Clear the screen and print the board.
"""
def printBoard():
    os.system('cls' if os.name == 'nt' else 'clear')
    print('\n'.join(['-'.join([value for key,value in row.items()]) for row in reversed(board)]) + '\n')


"""
Print an error message, wait for user to confirm, and redraw the board.
"""
def error( message ):
    input(message + ' . . .')
    printBoard()


"""
Prompt user for a move and validate that move.
"""
def makeMove( piece, board ):
    while True:
        move = input(f'player {piece}: ')

        # move must not be empty
        if move == "":
            error("please input a move")
            continue

        # move must be <char><int> in the board size
        if int(move[1:]) not in range(1, args.size+1) or move[0] not in list(string.ascii_lowercase[:args.size]):
            error("move not on board")
            continue
        
        # space must be empty
        if board[int(move[1:])-1][move[0]] is not '|':
            error('invalid move')
            continue

        if move:
            break

    board[int(move[1:])-1][move[0]] = piece
    
def main():
    # black goes first
    black = True

    while True:

        printBoard()

        piece = u'\u25cb' if black else u'\u25cf'
        makeMove(piece, board)

        # alternative turns between players
        black = not black


if __name__ == "__main__":
    main()
